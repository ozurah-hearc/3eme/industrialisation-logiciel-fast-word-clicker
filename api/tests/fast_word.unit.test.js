const FastWord = require("../src/fast_word.js");

function getGame() {
    const fw = new FastWord();
    fw.theme_id = 0;
    fw.words = ["test", "test2", "test3"];
    fw.combo = 0;
    fw.score = 0;
    fw.game_id = 0;
    return fw;
}

test("Is FastWord defined", () => {
    expect(FastWord).toBeDefined();
});

test("Is a game created", async () => {
    const game = await FastWord.createGame(0);
    expect(game).toBeDefined();
    expect(game.game_id).toBeGreaterThan(0);
});

test("Is a word verified, right word too fast", async () => {
    const game = getGame();
    try {
        game.verifyWord("test", 0);
    } catch (e) {
        expect(e.message).toBe("Time must be positive");
    }
});

test("Is a word verified, right word fast", async () => {
    const game = getGame();
    const score = game.verifyWord("test", 1);
    expect(score.correct).toBeTruthy();
    expect(score.score).toBe(3000);
    expect(score.combo).toBe(1);
});

test("Is a word verified, right word slow", async () => {
    const game = getGame();
    const score = game.verifyWord("test", 3000);
    expect(score.correct).toBeTruthy();
    expect(score.score).toBe(1);
    expect(score.combo).toBe(1);
});

test("Is a word verified, right word too slow", async () => {
    const game = getGame();
    const score = game.verifyWord("test", 3001);
    expect(score.correct).toBeTruthy();
    expect(score.score).toBe(0);
    expect(score.combo).toBe(0);
});

test("Is a word verified, wrong word", async () => {
    const game = getGame();
    const score = game.verifyWord("test4", 0);
    expect(score.correct).toBeFalsy();
    expect(score.score).toBe(0);
    expect(score.combo).toBe(0);
});

test("Is a word verified, wrong word after timeout", async () => {
    const game = getGame();
    game.combo = 1;
    const score = game.verifyWord("test4", 3001);
    expect(score.correct).toBeFalsy();
    expect(score.score).toBe(0);
    expect(score.combo).toBe(1);
});
