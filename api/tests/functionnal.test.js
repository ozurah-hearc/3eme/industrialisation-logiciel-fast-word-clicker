const webdriver = require("selenium-webdriver");
const firefox = require("selenium-webdriver/firefox");
const server = require("../src/index.js");

const TIMEOUT = 20000;

function getDriver() {
    return new webdriver.Builder()
        .forBrowser("firefox")
        .setFirefoxOptions(new firefox.Options().headless())
        .build();
}

afterAll(async () => {
    server.close();
});

// valiidation test
test(
    "Is google.com working",
    async () => {
        const driver = getDriver();
        await driver.get("https://www.google.com");
        const title = await driver.getTitle();
        expect(title).toBe("Google");
        await driver.quit();
    },
    TIMEOUT
);

test(
    "Is fastword working",
    async () => {
        const driver = getDriver();
        await driver.get("http://localhost:3000");
        const content = await driver
            .findElement(webdriver.By.css("body"))
            .getText();
        expect(content).toBe("OK");
        await driver.quit();
    },
    TIMEOUT
);

test.each(["themes", "words"])(
    "Are %s loaded and constant",
    async (element) => {
        const driver = getDriver();
        await driver.get("http://localhost:3000/api/" + element);
        const content = await driver
            .findElement(webdriver.By.id("json"))
            .getText();
        expect(content).toMatchSnapshot();
        await driver.quit();
    },
    TIMEOUT
);

test(
    "Are words for each theme loaded and constant",
    async () => {
        const driver = getDriver();
        await driver.get("http://localhost:3000/api/themes");
        const themesJson = await driver
            .findElement(webdriver.By.id("json"))
            .getText();
        const themes = JSON.parse(themesJson);
        for (let theme of themes) {
            await driver.get("http://localhost:3000/api/words/" + theme.id);
            const content = await driver
                .findElement(webdriver.By.id("json"))
                .getText();
            expect(content).toMatchSnapshot();
        }
        await driver.quit();
    },
    TIMEOUT
);
