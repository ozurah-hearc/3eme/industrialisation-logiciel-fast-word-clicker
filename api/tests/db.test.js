
const {Theme, Game, Word} = require("../src/db.js");

test("Is Theme defined", () => {
    expect(Theme).toBeDefined();
});

test("Is Game defined", () => {
    expect(Game).toBeDefined();
});

test("Is Word defined", () => {
    expect(Word).toBeDefined();
});

test("DB has data", async () => {
    const themes = await Theme.findAll();
    const words = await Word.findAll();
    expect(themes.length).toBeGreaterThan(0);
    expect(words.length).toBeGreaterThan(0);
});

it("Themes don't change", async () => {
    const rows = await Theme.findAll();
    
    rows.forEach((row) => {
        expect(row.theme).toMatchSnapshot();
    });    
});


it("Words count is correct", async () => {
    const themesNumber = 5;
    for( let themeId = 1; themeId <= themesNumber; themeId++) {
        const number = await Word.count({where: {theme_id: themeId}});
        expect(number).toBe(10);
    }
});