const e = require("express");
const FastWord = require("../src/fast_word.js");

function getGame() {
    const fw = new FastWord();
    fw.theme_id = 0;
    fw.words = ["test", "test2", "test3"];
    fw.combo = 0;
    fw.score = 0;
    fw.game_id = 0;
    return fw;
}

test("Is combo incremented", async () => {
    const game = getGame();
    game.verifyWord("test", 1);
    expect(game.combo).toBe(1);
    game.verifyWord("test2", 1);
    expect(game.combo).toBe(2);
    game.verifyWord("test3", 1);
    expect(game.combo).toBe(3);
});

test("Is combo reset", async () => {
    const game = getGame();
    game.verifyWord("test", 1);
    expect(game.combo).toBe(1);
    game.verifyWord("test2", 1);
    expect(game.combo).toBe(2);
    game.verifyWord("test4", 1);
    expect(game.combo).toBe(0);
});

test("Game life cycle", async () => {
    const game = await FastWord.createGame(1);
    expect(game).toBeDefined();
    game.verifyWord("Book", 1);
    expect(game.combo).toBe(1);
    game.saveGame();

    const game2 = await FastWord.getGame(game.game_id);
    expect(game2).toBeDefined();
    expect(game2.combo).toBe(game.combo);
    expect(game2.score).toBe(game.score);
    expect(game2.theme_id).toBe(game.theme_id);
    expect(game2.words).toEqual(game.words);
});