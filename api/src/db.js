const { readFileSync } = require("fs");
const path = require("path");
const { Sequelize, DataTypes } = require("sequelize");

const db = new Sequelize({
    dialect: "sqlite",
    storage: "./db.sqlite",
    logging: false,
});

const Theme = db.define("theme", {
    theme: DataTypes.STRING,
});

const Game = db.define("game", {
    theme_id: DataTypes.INTEGER,
    combo: DataTypes.INTEGER,
    score: DataTypes.INTEGER,
});

const Word = db.define("word", {
    theme_id: DataTypes.INTEGER,
    word: DataTypes.STRING,
});

async function init() {
    await db.sync();

    const themes = await Theme.findAll();
    if (themes.length == 0) {
        await Theme.bulkCreate([
            { theme: "Study" },
            { theme: "Weather" },
            { theme: "Clothes" },
            { theme: "Food" },
            { theme: "Animals" },
        ]);
        const Data = JSON.parse(readFileSync(path.resolve(__dirname, "../data/words.json")));
        const themes = await Theme.findAll();
        themes.forEach(async (theme) => {
            await Data[theme.theme].forEach(async (word) => {
                await Word.create({ theme_id: theme.id, word: word });
            });
        });
    }
}

init();

module.exports = { Theme, Game, Word };
