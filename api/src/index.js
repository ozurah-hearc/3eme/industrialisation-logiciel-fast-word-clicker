const express = require("express");
const cors = require("cors");
const config = require("dotenv").config();


const bodyParser = require("body-parser");

const FastWord = require("./fast_word.js");

const app = express();

app.disable("x-powered-by");

let corsOptions = {
    origin: process.env.CORS_ORIGIN || "http://localhost:8080",
    optionsSuccessStatus: 200,
};


app.use(cors(corsOptions));

app.use(bodyParser.json());

const port = 3000;

app.get("/", (req, res) => {
    res.send("OK");
});

app.get("/api/themes", async (req, res) => {
    res.send(await FastWord.getThemes());
});

app.get("/api/words", async (req, res) => {
    res.send(await FastWord.getAllWords());
});

app.get("/api/words/:theme_id", async (req, res) => {
    res.send(await FastWord.getWords(req.params.theme_id));
});

app.post("/api/game", async (req, res) => {
    const game = await FastWord.createGame(req.body.theme_id);
    res.status(201).send(game);
});

app.post("/api/game/:game_id", async (req, res) => {
    const game = await FastWord.getGame(req.params.game_id);
    const newScore = game.verifyWord(req.body.word, req.body.time);
    game.saveGame();
    res.status(200).send(newScore);
});

let server = app.listen(port, () => {
    console.log(`FastWordAPI listening at http://localhost:${port}`);
});

module.exports = server;
