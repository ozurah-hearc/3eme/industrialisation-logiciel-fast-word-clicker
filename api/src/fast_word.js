const { Theme, Game, Word } = require("./db.js");
const assert = require("assert");

class FastWord {
    TIMEOUT = 3000;

    constructor() {
        this.theme_id = 0;
        this.words = [];
        this.combo = 0;
        this.score = 0;
        this.game_id = 0;
    }
    static async getGame(game_id) {
        const dbGame = await Game.findOne({ where: { id: game_id } });
        const game = new FastWord();
        game.theme_id = dbGame.theme_id;
        game.words = await this.getWords(dbGame.theme_id);
        game.combo = dbGame.combo;
        game.score = dbGame.score;
        game.game_id = dbGame.id;
        return game;
    }

    static async getThemes() {
        return await Theme.findAll({ attributes: ["id", "theme"] });
    }

    static async getWords(theme_id) {
        return (
            await Word.findAll({
                attributes: ["word"],
                where: { theme_id: theme_id },
            })
        ).map((word) => {
            word = word.word;
            return word;
        });
    }

    static async getAllWords() {
        return (await Word.findAll({ attributes: ["word"] })).map((word) => {
            word = word.word;
            return word;
        });
    }

    static async createGame(theme_id) {
        const game = new FastWord();
        game.theme_id = theme_id;
        game.words = await this.getWords(theme_id);
        game.game_id = (
            await Game.create({ theme_id: theme_id, combo: 0, score: 0 })
        ).id;

        return game;
    }

    verifyWord(word, time) {
        assert(time >= 0, "Time must be positive");
        const correct = this.words.includes(word);
        if (correct && time <= this.TIMEOUT) {
            this.combo++;
            this.score += Math.ceil(this.TIMEOUT / time) * this.combo;
        } else if (correct || (!correct && time <= this.TIMEOUT)) {
            this.combo = 0;
        }
        return { score: this.score, combo: this.combo, correct: correct };
    }

    saveGame() {
        Game.update(
            { combo: this.combo, score: this.score },
            { where: { id: this.game_id } }
        );
    }
}

module.exports = FastWord;
