To use jsdom with Jest, we need to add at the top of the test file:

```js
/**
 * @jest-environment jsdom
 */
```

**JSDom + Mock source**
- https://jestjs.io/docs/configuration
- https://github.com/jsdom/jsdom

Note :
- to be able to tests variables of the scripts, we have add anonymous function for setter/getter in the script.js module exports
- Without thats, assign a value to a variable in the test will override the reference of the var and tests will fails
- sample : `script.var1` = 0 ===>> we won't be able to detect if var1 is changed by the script, because the reference is changed

# BeforeAll
We use beforeAll to load the html file and the script.js file before each test
We also set a mock for the canvas, because the canvas.getContext doesn't exist in jsdom

```js
const fs = require("fs"); // Note : Can't be ES import, only CommonJS

let script;

beforeAll(() => {
    let fileContent = fs.readFileSync("./tests/test.dom.html", "utf8");
    document.body.innerHTML = fileContent;

    // Mock canvas (without define this, the canvas.getContext rise an error)
    // To add "used" function, add it in the mock, see https://github.com/jsdom/jsdom/issues/1782#issuecomment-337656878
    HTMLCanvasElement.prototype.getContext = () => {
        return {
            fillRect: function () {},
            clearRect: function () {},
        };
    };

    script = require("../src/script.js");
    console.log(script); // To check available functions/variables to test
});
```

# Samples tests
We have 3 samples tests in `unit.test.js` file, this tests is just to show how to test the script.js file + be sure we can assign value to a variable in script without override the reference of the var
- `samples test`
- `assign default value to var`
