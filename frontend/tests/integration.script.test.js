/**
 * @jest-environment jsdom
 */

"use strict";

const fs = require("fs"); // Note : Can't be ES import, only CommonJS

let script;

beforeAll(() => {
    let fileContent = fs.readFileSync("./tests/test.dom.html", "utf8");
    document.body.innerHTML = fileContent;

    // Mock canvas (without define this, the canvas.getContext rise an error)
    // To add "used" function, add it in the mock, see https://github.com/jsdom/jsdom/issues/1782#issuecomment-337656878
    HTMLCanvasElement.prototype.getContext = () => {
        return {
            fillRect: function () {},
            clearRect: function () {},
        };
    };

    script = require("../src/script.js");
    console.log(script); // To check available functions/variables to test
});

test("multiple appendWord", async () => {
    script.setWordsMap({}); // Without reset, the value can be changed in another test

    let word = "test1";
    let id = script.appendWord(word);
    expect(script.getWordsMap()).toEqual({ [id]: { txt: word } });

    let word2 = "test2";
    let id2 = script.appendWord(word2);
    expect(script.getWordsMap()).toEqual({
        [id]: { txt: word },
        [id2]: { txt: word2 },
    });
});

test("multiple removeWord", async () => {
    script.setWordsMap({
        10: { txt: "test" },
        22: { txt: "test2" },
        100: { txt: "test3" },
    });

    script.removeWord(22);
    expect(script.getWordsMap()).toEqual({
        10: { txt: "test" },
        100: { txt: "test3" },
    });

    script.removeWord(10);
    expect(script.getWordsMap()).toEqual({ 100: { txt: "test3" } });
});

test("add and then remove word", async () => {
    script.setWordsMap({}); // Without reset, the value can be changed in another test

    let word = "test1";
    let id = script.appendWord(word);
    expect(script.getWordsMap()).toEqual({ [id]: { txt: word } });

    // because word ID is based on game time, we "fake" the game time
    // (convention over configuration)

    script.setGameTimeStart(1000);

    let word2 = "test2";
    let id2 = script.appendWord(word2);
    expect(script.getWordsMap()).toEqual({
        [id]: { txt: word },
        [id2]: { txt: word2 },
    });

    console.log("test =>", script.getWordsMap());

    script.removeWord(id);
    expect(script.getWordsMap()).toEqual({ [id2]: { txt: word2 } });

    script.removeWord(id2);
    expect(script.getWordsMap()).toEqual({});
});

test("multiple modification score for highscore", async () => {
    script.setScore(0); // Without reset, the value can be changed in another test

    script.setScore(10);
    script.determinateMaxScore();

    expect(script.getHighScore()).toBe(10);

    script.setScore(5);
    script.determinateMaxScore();

    expect(script.getHighScore()).toBe(10);

    script.setScore(15);
    script.determinateMaxScore();

    expect(script.getHighScore()).toBe(15);
});
