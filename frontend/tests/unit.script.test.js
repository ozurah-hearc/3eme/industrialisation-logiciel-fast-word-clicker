/**
 * @jest-environment jsdom
 */

"use strict";

const fs = require("fs"); // Note : Can't be ES import, only CommonJS

let script;

beforeAll(() => {
    let fileContent = fs.readFileSync("./tests/test.dom.html", "utf8");
    document.body.innerHTML = fileContent;

    // Mock canvas (without define this, the canvas.getContext rise an error)
    // To add "used" function, add it in the mock, see https://github.com/jsdom/jsdom/issues/1782#issuecomment-337656878
    HTMLCanvasElement.prototype.getContext = () => {
        return {
            fillRect: function () {},
            clearRect: function () {},
        };
    };

    script = require("../src/script.js");
    console.log(script); // To check available functions/variables to test
});

test("samples test", async () => {
    const combo = document.getElementById("comboStreak");
    expect(combo).not.toBeNull();

    const result = script.getRandomInt(10);
    console.log(result);
    expect(result).toBeGreaterThanOrEqual(0);
    expect(result).toBeLessThanOrEqual(9);

    console.log(script.themeList);
    script.getThemeList()[0] = "test";
    console.log(script.getThemeList());
});

test("assign default value to var TEST1", async () => {
    let word = "test1";
    let id = script.appendWord(word);
    expect(script.getWordsMap()).toEqual({ [id]: { txt: word } });

    script.setWordsMap({});

    word = "test2";
    id = script.appendWord(word);
    expect(script.getWordsMap()).toEqual({ [id]: { txt: word } });
});

test("assign default value to var TEST2", async () => {
    script.setComboStreak(10);
    expect(script.getComboStreak()).toBe(10);

    await script.initGame();
    expect(script.getComboStreak()).toBe(0);
});

test("initGame should reset game var except highscore", async () => {
    script.setComboStreak(10);
    script.setScore(10);
    script.setHighScore(10);
    script.setWordsMap({ 20: { txt: "test" }, 30: { txt: "test2" } });
    script.setWords(["test", "test2"]);

    // - script.timeGameStart is reset by "startGame" (not tested here)
    // - We can't test if gameloopInterval is cleared (there is no built-in function to check it)

    await script.initGame();

    expect(script.getComboStreak()).toBe(0);
    expect(script.getScore()).toBe(0);
    expect(script.getWordsMap()).toEqual({});
    expect(script.getWords()).toEqual([]);
});

test("updateHUD should update the HUD", async () => {
    script.setHighScore(10);
    script.setScore(10);
    script.setComboStreak(10);

    await script.updateHUD();

    expect(document.getElementById("comboStreak").innerHTML).toBe("10");
    expect(document.getElementById("score").innerHTML).toBe("10");
    expect(document.getElementById("highScore").innerHTML).toBe("10");
});

test("appendWord should add a word to the wordsMap and return the id", async () => {
    script.setWordsMap({}); // Without reset, the value can be changed in another test

    const id = script.appendWord("test");

    expect(script.getWordsMap()).toEqual({ [id]: { txt: "test" } });
});

test("removeWord should remove a word from the wordsMap and return the time passed before removed", async () => {
    script.setWordsMap({ 10: { txt: "test" } });

    const timePassed = script.removeWord(10);

    expect(script.getWordsMap()).toEqual({});
    expect(timePassed).toBeGreaterThanOrEqual(0);
});

test("getRandomInt should return a random int between 0 and the max value", async () => {
    for (let i = 0; i < 100; i++) {
        const result = script.getRandomInt(3);
        expect(result).toBeGreaterThanOrEqual(0);
        expect(result).toBeLessThanOrEqual(2);
    }
});

test("getRandomIntRange should return a random int between the min and max value", async () => {
    for (let i = 0; i < 100; i++) {
        const result = script.getRandomIntRange(1, 3);
        expect(result).toBeGreaterThanOrEqual(1);
        expect(result).toBeLessThanOrEqual(3);
    }
});

test("getGameTime should return the time passed since the start of the game", async () => {
    script.setGameTimeStart(Date.now() - 1000);

    expect(script.getGameTime()).toBeGreaterThanOrEqual(1000);
    expect(script.getGameTime()).toBeLessThanOrEqual(1100); // margin to avoid false negative due to execution time
});

test("determinateMaxScore #1 should return the max score possible", async () => {
    script.setScore(10);
    script.setHighScore(5);

    script.determinateMaxScore();

    expect(script.getHighScore()).toEqual(10);
});

test("determinateMaxScore #2 should return the max score possible", async () => {
    script.setScore(10);
    script.setHighScore(15);

    script.determinateMaxScore();

    expect(script.getHighScore()).toEqual(15);
});
