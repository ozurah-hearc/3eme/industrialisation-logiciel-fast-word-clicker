/**
 * @jest-environment jsdom
 */

"use strict";

const fs = require("fs"); // Note : Can't be ES import, only CommonJS

test("index have required elements", async () => {
    let fileContent = fs.readFileSync("./src/index.html", "utf8");
    document.body.innerHTML = fileContent;

    expect(document.getElementById("comboStreak")).not.toBeNull();

    expect(document.getElementById("score")).not.toBeNull();
    expect(document.getElementById("highScore")).not.toBeNull();

    expect(document.getElementById("choiceTheme")).not.toBeNull();

    expect(document.getElementById("canvas")).not.toBeNull();
    expect(document.getElementById("choiceTheme")).not.toBeNull();
});
