const express = require("express");

const host = process.env.HOST || "http://localhost";
const port = 8080;

const server = express().use("/", express.static(__dirname + "/src")).listen(port, () => {
    console.log(`FastWord listening at ${host}:${port}`);
});

module.exports = server;
