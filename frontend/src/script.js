"use strict";
const API_URL = "http://" + window.location.hostname + ":3000/api/";

// Variables

// - Feeded by API
let themeList = {}; // { {id:, theme:} }
let words = [];

// - const Game config
const GAMELOOP_SPEED_MS = 1000;
const MIN_DELAY_MS_BETWEEN_SPAWN = 1000;
const MAX_DELAY_MS_BETWEEN_SPAWN = 4000;
const MARGIN_TIME_TO_REMOVE_MS = 2000;
const boxColors = ["#FFCCCC", "#CCFFCC", "#CCCCFF"];

const BOX_MARGIN_X = 20;
const BOX_MARGIN_Y = 10;
const FONT_CONFIG = "24px Arial";

// - Game config
let gameId = 0;
let wordTimeout = 10000;
let timeGameStart = 0;
let delaySpawn = 0;
let wordsMap = {}; // Data of the words (text, position, color, time of spawn, etc.)

let gameloopInterval = null;

// - Game stats
let comboStreak = 0;
let score = 0;
let highScore = 0;

// Components

const canvas = document.getElementById("canvas");
const ctx = canvas.getContext("2d");
const btnStart = document.getElementById("startGame");
const cboxThemeChoice = document.getElementById("choiceTheme");
const lblComboStreak = document.getElementById("comboStreak");
const lblScore = document.getElementById("score");
const lblHighScore = document.getElementById("highScore");

// Functions that use the API
const fetchThemes = async () => {
    await fetch(API_URL + "themes")
        .then((response) => response.json())
        .then((data) => {
            themeList = data;
            console.log("Fetchs theme finished");
        })
        .catch((error) => {
            themeList = {};
            console.log(error);
        });
};

const fetchWords = async () => {
    console.log("Fetch words");
    await fetch(API_URL + "words")
        .then((response) => response.json())
        .then((data) => {
            console.log(data);
            words = data;
        })
        .catch((error) => {
            console.log(error);
        });
};

const createAndFetchGame = async (themeId) => {
    // Post header source : https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch#supplying_request_options
    await fetch(API_URL + "game", {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify({ theme_id: themeId }),
    })
        .then((response) => response.json())
        .then((data) => {
            console.log(data);
            // TIMEOUT = max react time for the word
            // combo
            // score
            // game_id
            // theme_id
            // words : [] = words of the theme

            wordTimeout = data.TIMEOUT + MARGIN_TIME_TO_REMOVE_MS;
            comboStreak = data.combo;
            score = data.score;
            gameId = data.game_id;
            // No need to handle theme_id and words, data are calculated on backend

            console.log("Game created");
        })
        .catch((error) => {
            console.log(error);
        });
};

const sendWord = async (word, time) => {
    await fetch(API_URL + "game/" + gameId, {
        method: "POST",
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify({ word: word, time: time }),
    })
        .then((response) => response.json())
        .then((data) => {
            console.log(data);
            // score
            // combo
            // correct

            score = data.score;
            comboStreak = data.combo;

            determinateMaxScore();
        })
        .catch((error) => {
            console.log(error);
        });
};

// Functions
/**
 * Function to call when the html page is loaded
 * Note : call by the html page
 */
async function initPage() {
    await fetchThemes();

    cboxThemeChoice.innerHTML = "";

    for (const [, value] of Object.entries(themeList)) {
        let option = document.createElement("option");
        option.value = value.id;
        option.text = value.theme;
        cboxThemeChoice.appendChild(option);
    }

    highScore = 0;

    initGame();
    updateHUD();
}

function initGame() {
    console.log("Game configuration");

    if (gameloopInterval !== null) {
        console.log("A game was running => it will be stopped");
        clearTimeout(gameloopInterval);
    }

    comboStreak = 0;
    score = 0;

    // Do not use wordsMap = {} because it will not remove the event listeners
    for (let id in wordsMap) {
        removeWord(id);
    }
    words = [];

    ctx.clearRect(0, 0, canvas.width, canvas.height);

    console.log("Game init finished => ready to start the game");
}

function updateHUD() {
    lblComboStreak.innerHTML = comboStreak;
    lblScore.innerHTML = score;
    lblHighScore.innerHTML = highScore;
}

/**
 * Create a game in backend and start it in frontend
 */
async function startGame() {
    fetchWords(); // No need of await, the game can start without the words and can change in runtime
    // fetchWords is placed before createAndFetchGame to already start the fetch

    await createAndFetchGame(cboxThemeChoice.value);

    console.log("Game created in backend");

    console.log("C'est l'heure du du-du-du-el !");

    timeGameStart = Date.now();

    gameloop();
}

/**
 * Add a word to the map and return it's id (the time it was added)
 */
function appendWord(word) {
    let riseTime = getGameTime();
    wordsMap[riseTime] = { txt: word };

    return riseTime;
}

/**
 * Remove a word from the map and return the time it took to remove it
 */
function removeWord(riseTime) {
    // Unregistry event of the word in canvas
    if (wordsMap[riseTime].canvasOnClick)
        canvas.removeEventListener("click", wordsMap[riseTime].canvasOnClick);

    if (wordsMap[riseTime].onAutoRemove)
        clearTimeout(wordsMap[riseTime].onAutoRemove);

    delete wordsMap[riseTime];
    return getGameTime() - riseTime;
}

function getRandomInt(max) {
    // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random
    return Math.floor(Math.random() * max);
}

function getRandomIntRange(min, max) {
    return getRandomInt(max - min) + min;
}

function getGameTime() {
    return Date.now() - timeGameStart;
}

function gameloop() {
    gameloopInterval = setTimeout(gameloop, GAMELOOP_SPEED_MS);

    delaySpawn -= GAMELOOP_SPEED_MS;

    if (delaySpawn <= 0) {
        spawnWord();

        // Randomize next spawn (to be not too predictable)
        delaySpawn = getRandomIntRange(
            MIN_DELAY_MS_BETWEEN_SPAWN,
            MAX_DELAY_MS_BETWEEN_SPAWN
        );
    }
}

function spawnWord() {
    if (words.length === 0) {
        console.log("No words to spawn");
        return;
    }

    let word = words[Math.floor(Math.random() * words.length)];

    let wordId = appendWord(word);

    startAutoremoveWord(wordId);

    cvCreateWord(wordId, word);

    repaintCanvas();
}

function startAutoremoveWord(wordId) {
    wordsMap[wordId].onAutoRemove = setTimeout(function () {
        const word = wordsMap[wordId].txt;
        const elapsedTime = removeWord(wordId);
        console.log("Word " + word + " removed after " + elapsedTime + "ms");

        sendWord(word, elapsedTime).then(() => {
            updateHUD();
        });

        repaintCanvas();
    }, wordTimeout);
}

/**
 * Create a clickable word on the canvas
 * @param {int} id ID of the word in the wordsMap
 * @param {string} word Word to display
 */
function cvCreateWord(id, word) {
    ctx.font = FONT_CONFIG;

    const wordSize = ctx.measureText(word);
    const wordWidth = wordSize.width;
    const wordHeight =
        wordSize.actualBoundingBoxAscent + wordSize.actualBoundingBoxDescent;

    const wordX = getRandomIntRange(
        BOX_MARGIN_X,
        canvas.width - wordWidth - BOX_MARGIN_X
    ); // Left
    const wordY = getRandomIntRange(
        wordHeight + BOX_MARGIN_Y,
        canvas.height - BOX_MARGIN_Y
    ); // Bottom of the text

    const boxLeft = wordX - BOX_MARGIN_X;
    const boxWidth = wordWidth + BOX_MARGIN_X * 2;
    const boxTop = wordY - wordHeight - BOX_MARGIN_Y;
    const boxHeight = wordHeight + BOX_MARGIN_Y * 2;

    console.log("word sizes : (" + wordWidth + ", " + wordHeight + ")");
    console.log(
        "left-top corner of Word " +
            word +
            " at pos (" +
            wordX +
            ", " +
            wordY +
            ")"
    );

    wordsMap[id].boxLeft = boxLeft;
    wordsMap[id].boxTop = boxTop;
    wordsMap[id].boxWidth = boxWidth;
    wordsMap[id].boxHeight = boxHeight;

    wordsMap[id].boxColor = boxColors[getRandomInt(boxColors.length)];

    wordsMap[id].wordX = wordX;
    wordsMap[id].wordY = wordY;

    console.log(wordsMap[id]);

    // Event on the word
    const wordclick = function (event) {
        const onClickFunction = wordsMap[id].canvasOnClick;

        //event.stopPropagation(); // TODO Seem's to not work for content inside canvas => clickInProgress

        const x = event.pageX - canvas.offsetLeft;
        const y = event.pageY - canvas.offsetTop;

        /***** Guard close *****/

        // Outside of the box
        if (
            x < boxLeft ||
            x > boxLeft + boxWidth ||
            y < boxTop ||
            y > boxTop + boxHeight
        ) {
            return;
        }

        // if there is not more recents words that overlapping the event of the word
        if (
            Object.entries(wordsMap).some(
                ([key, wordData]) =>
                    key > id &&
                    x >= wordData.boxLeft &&
                    x <= wordData.boxLeft + wordData.boxWidth &&
                    y >= wordData.boxTop &&
                    y <= wordData.boxTop + wordData.boxHeight
            )
        ) {
            console.log("Overlap => Stop propagation");
            return;
        }

        /***** Remove Logic *****/
        const elapsedTime = removeWord(id);

        console.log("Clicked on " + word + " id " + id);
        console.log("Clicked " + elapsedTime + "ms after spawn");

        sendWord(word, elapsedTime).then(() => {
            updateHUD();
        });

        repaintCanvas();

        console.log(wordsMap);

        canvas.removeEventListener("click", onClickFunction);
    };

    wordsMap[id].canvasOnClick = wordclick;
    canvas.addEventListener("click", wordclick);
}

function repaintCanvas() {
    ctx.clearRect(0, 0, canvas.width, canvas.height);

    for (let id in wordsMap) {
        let wordData = wordsMap[id];

        // Rectangle
        ctx.fillStyle = wordData.boxColor;
        ctx.strokeStyle = "#000";
        ctx.lineWidth = 1;
        ctx.fillRect(
            wordData.boxLeft,
            wordData.boxTop,
            wordData.boxWidth,
            wordData.boxHeight
        );
        ctx.strokeRect(
            wordData.boxLeft,
            wordData.boxTop,
            wordData.boxWidth,
            wordData.boxHeight
        );

        // Text
        ctx.fillStyle = "#000";
        ctx.fillText(wordData.txt, wordData.wordX, wordData.wordY);

        // Debug
        // ctx.fillStyle = "#f0f";
        // ctx.fillRect(wordData.wordX, wordData.wordY, 3, 3);
    }
}

function determinateMaxScore() {
    highScore = Math.max(score, highScore);
}

// Events
btnStart.addEventListener("click", function () {
    initGame();
    updateHUD();
    startGame(); // No need to await, the game will start in background
});

if (typeof module !== "undefined") {
    // To be able to tests "vanilla" js; we need to export the functions we want to test
    // https://www.alanwsmith.com/posts/how-to-use-jest-to-test-vanilla-javascript-files-without-a-framework--23yct7ahlbg3
    // After many local attempts, to be able to modify variables, we need to add "getter/setter" functions

    // Variables
    module.exports.setHighScore = (value) => { highScore = value; };
    module.exports.getHighScore = () => { return highScore; };

    module.exports.setScore = (value) => { score = value; };
    module.exports.getScore = () => { return score; };

    module.exports.setComboStreak = (value) => { comboStreak = value; };
    module.exports.getComboStreak = () => { return comboStreak; };

    module.exports.setWordsMap = (value) => { wordsMap = value; };
    module.exports.getWordsMap = () => { return wordsMap; };

    module.exports.setWords = (value) => { words = value; };
    module.exports.getWords = () => { return words; };

    module.exports.setThemeList = (value) => { themeList = value; };
    module.exports.getThemeList = () => { return themeList; };

    module.exports.setGameTimeStart = (value) => { timeGameStart = value; };
    // Get is a function that exists in our code, no need to make a anonymous function


    // Functions
    module.exports.initGame = initGame;
    module.exports.updateHUD = updateHUD;
    module.exports.appendWord = appendWord;
    module.exports.removeWord = removeWord;
    module.exports.getRandomInt = getRandomInt;
    module.exports.getRandomIntRange = getRandomIntRange;
    module.exports.getGameTime = getGameTime;
    module.exports.determinateMaxScore = determinateMaxScore;

    // Non testable functions :
    //      fetchThemes; fetchWords; createAndFetchGame, sendWord
    //    => use the backend API
    //
    //      initPage, startGame, startAutoremoveWord
    //    => call a function which use backend API
    //      note : to simulate startGame, just set "timeGameStart = Date.now();""
    //
    //      gameloop
    //    => call no realy able to test, because use timeout and randoms (+ use spawnWord)
    //
    //      cvCreateWord, repaintCanvas
    //    => use the canvas, which is not available in our test environment
    //
    //      spawnWord
    //    => use cvCreateWord; repaintCanvas, startAutoremoveWord, + randoms

}
